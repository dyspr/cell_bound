var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var initSize = 0.1
var dimension = 7
var boardArray = create2DArray(dimension, dimension, 0, false)
var direction = 0
var clusters = calculateClusters(getCells(boardArray))
var vertices = []
var player = {
  x: 0,
  y: 0
}
var frame = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
  for (var i = 0; i < clusters.length; i++) {
    vertices.push(getEmptyNeighbours(clusters[i]))
  }
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < clusters.length; i++) {
    for (var j = 0; j < clusters[i].length; j++) {
      push()
      translate(windowWidth * 0.5 + (clusters[i][j][0] - Math.floor(dimension * 0.5)) * boardSize * initSize, windowHeight * 0.5 + (clusters[i][j][1] - Math.floor(dimension * 0.5)) * boardSize * initSize)
      fill(64)
      fill(128 + 128 * ((i + 1) / clusters.length) * sin(frameCount * 0.05 + i * 0.25 + j * 0.05))
      noStroke()
      rect(0, 0, boardSize * initSize *  0.5, boardSize * initSize * 0.5)
      fill(128 - 128 * ((i + 1) / clusters.length) * sin(frameCount * 0.05 + i * 0.25 + j * 0.05))
      noStroke()
      ellipse(0, 0, boardSize * initSize *  0.25, boardSize * initSize * 0.25)
      pop()
    }
  }

  for (var i = 0; i < vertices.length; i++) {
    for (var j = 0; j < vertices[i].length; j++) {
      if (vertices[i][j][2] === 0) {
        push()
        translate(windowWidth * 0.5 + (vertices[i][j][0] - Math.floor(dimension * 0.5)) * boardSize * initSize, windowHeight * 0.5 + (vertices[i][j][1] - Math.floor(dimension * 0.5)) * boardSize * initSize)
        noFill()
        stroke(255)
        strokeWeight(boardSize * initSize * 0.05)
        strokeCap(ROUND)
        line(boardSize * initSize * 0.5, -boardSize * initSize * 0.5, boardSize * initSize * 0.5, boardSize * initSize * 0.5)
        pop()
      }
      if (vertices[i][j][2] === 1) {
        push()
        translate(windowWidth * 0.5 + (vertices[i][j][0] - Math.floor(dimension * 0.5)) * boardSize * initSize, windowHeight * 0.5 + (vertices[i][j][1] - Math.floor(dimension * 0.5)) * boardSize * initSize)
        noFill()
        stroke(255)
        strokeWeight(boardSize * initSize * 0.05)
        strokeCap(ROUND)
        line(-boardSize * initSize * 0.5, -boardSize * initSize * 0.5, -boardSize * initSize * 0.5, boardSize * initSize * 0.5)
        pop()
      }
      if (vertices[i][j][2] === 2) {
        push()
        translate(windowWidth * 0.5 + (vertices[i][j][0] - Math.floor(dimension * 0.5)) * boardSize * initSize, windowHeight * 0.5 + (vertices[i][j][1] - Math.floor(dimension * 0.5)) * boardSize * initSize)
        noFill()
        stroke(255)
        strokeWeight(boardSize * initSize * 0.05)
        strokeCap(ROUND)
        line(-boardSize * initSize * 0.5, boardSize * initSize * 0.5, boardSize * initSize * 0.5, boardSize * initSize * 0.5)
        pop()
      }
      if (vertices[i][j][2] === 3) {
        push()
        translate(windowWidth * 0.5 + (vertices[i][j][0] - Math.floor(dimension * 0.5)) * boardSize * initSize, windowHeight * 0.5 + (vertices[i][j][1] - Math.floor(dimension * 0.5)) * boardSize * initSize)
        noFill()
        stroke(255)
        strokeWeight(boardSize * initSize * 0.05)
        strokeCap(ROUND)
        line(-boardSize * initSize * 0.5, -boardSize * initSize * 0.5, boardSize * initSize * 0.5, -boardSize * initSize * 0.5)
        pop()
      }
    }
  }

  frame += deltaTime * 0.005
  if (frame >= 1) {
    frame = 0
    direction = Math.floor(Math.random() * 4)
    if (direction === 0) {
      if (player.y < 1) {
        player.y = dimension - 1
      } else {
        player.y--
      }
    }
    if (direction === 1) {
      if (player.y > dimension - 2) {
        player.y = 0
      } else {
        player.y++
      }
    }
    if (direction === 2) {
      if (player.x < 1) {
        player.x = dimension - 1
      } else {
        player.x--
      }
    }
    if (direction === 3) {
      if (player.x > dimension - 2) {
        player.x = 0
      } else {
        player.x++
      }
    }
    boardArray[player.x][player.y] = (boardArray[player.x][player.y] + 1) % 2
    clusters = calculateClusters(getCells(boardArray))
    vertices = []
    for (var i = 0; i < clusters.length; i++) {
      vertices.push(getEmptyNeighbours(clusters[i]))
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function getEmptyNeighbours(cluster) {
  // directions: 0 (right), 1 (left), 2 (bottom), 3 (top)
  var neighbours = []
  for (var i = 0; i < cluster.length; i++) {
    var posX = cluster[i][0]
    var posY = cluster[i][1]
    if (posX > 0) {
      if (isArrayInArray(cluster, [posX - 1, posY]) === false && boardArray[posX - 1][posY] === 0) {
        neighbours.push([posX - 1, posY, 0])
      }
    } else {
      neighbours.push([posX - 1, posY, 0])
    }
    if (posX < dimension - 1) {
      if (isArrayInArray(cluster, [posX + 1, posY]) === false && boardArray[posX + 1][posY] === 0) {
        neighbours.push([posX + 1, posY, 1])
      }
    } else {
      neighbours.push([posX + 1, posY, 1])
    }
    if (posY > 0) {
      if (isArrayInArray(cluster, [posX, posY - 1]) === false && boardArray[posX][posY - 1] === 0) {
        neighbours.push([posX, posY - 1, 2])
      }
    } else {
      neighbours.push([posX, posY - 1, 2])
    }
    if (posY < dimension - 1) {
      if (isArrayInArray(cluster, [posX, posY + 1]) === false && boardArray[posX][posY + 1] === 0) {
        neighbours.push([posX, posY + 1, 3])
      }
    } else {
      neighbours.push([posX, posY + 1, 3])
    }
  }
  neighbours = multiDimensionalUnique(neighbours)
  return neighbours
}

function calculateClusters(cells) {
  var clusters = []
  for (var i = 0; i < cells.length; i++) {
    var initCell = cells[i]
    var cluster = []
    cluster.push(initCell)
    for (var k = 0; k < cells.length; k++) {
      var neighbours = getNeighbours(cluster)
      for (var j = 0; j < neighbours.length; j++) {
        cluster.push(neighbours[j])
      }
    }
    clusters.push(cluster)
    cluster = []
  }
  for (var i = 0; i < clusters.length; i++) {
    clusters[i].sort(function(a, b) {
      return a[0] < b[0] ? -1 : (a[0] == b[0] ? 0 : 1)
    })
    clusters[i].sort(function(a, b) {
      return a[1] < b[1] ? -1 : (a[1] == b[1] ? 0 : 1)
    })
  }
  clusters = multiDimensionalUnique(clusters)
  return clusters
}

function getCells(array) {
  var clusters = []
  var cells = []
  for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array[i].length; j++) {
      if (array[i][j] === 1) {
        cells.push([i, j])
      }
    }
  }
  return cells
}

function getNeighbours(cluster) {
  var neighbours = []
  for (var i = 0; i < cluster.length; i++) {
    var posX = cluster[i][0]
    var posY = cluster[i][1]
    if (posX > 0) {
      if (isArrayInArray(cluster, [posX - 1, posY]) === false && boardArray[posX - 1][posY] !== 0) {
        neighbours.push([posX - 1, posY])
      }
    }
    if (posX < dimension - 1) {
      if (isArrayInArray(cluster, [posX + 1, posY]) === false && boardArray[posX + 1][posY] !== 0) {
        neighbours.push([posX + 1, posY])
      }
    }
    if (posY > 0) {
      if (isArrayInArray(cluster, [posX, posY - 1]) === false && boardArray[posX][posY - 1] !== 0) {
        neighbours.push([posX, posY - 1])
      }
    }
    if (posY < dimension - 1) {
      if (isArrayInArray(cluster, [posX, posY + 1]) === false && boardArray[posX][posY + 1] !== 0) {
        neighbours.push([posX, posY + 1])
      }
    }
  }
  neighbours = multiDimensionalUnique(neighbours)
  return neighbours
}

function multiDimensionalUnique(arr) {
  var uniques = []
  var itemsFound = {}
  for (var i = 0, l = arr.length; i < l; i++) {
    var stringified = JSON.stringify(arr[i])
    if (itemsFound[stringified]) {
      continue
    }
    uniques.push(arr[i])
    itemsFound[stringified] = true
  }
  return uniques
}

function isArrayInArray(arr, item) {
  var item_as_string = JSON.stringify(item)
  var contains = arr.some(function(ele) {
    return JSON.stringify(ele) === item_as_string
  })
  return contains
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = Math.floor(Math.random() * 2)
      }
    }
    array[i] = columns
  }
  return array
}
